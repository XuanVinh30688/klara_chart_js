(function (Chartist) {
    const defaultOptions = {
        width: undefined,
        height: 400,
        chartPadding: {
            top: 15,
            right: 15,
            bottom: 5,
            left: 10
        },
        axisX: {
            offset: 30,
            position: 'end',
            labelOffset: {
                x: 0,
                y: 0
            },
            showLabel: true,
            showGrid: false,
            labelInterpolationFnc: Chartist.noop,
            scaleMinSpace: 30,
            onlyInteger: false
        },
        axisY: {
            offset: 40,
            position: 'start',
            labelOffset: {
                x: 0,
                y: 0
            },
            showLabel: true,
            showGrid: false,
            labelInterpolationFnc: Chartist.noop,
            scaleMinSpace: 20,
            onlyInteger: false
        },
        classNames: {
            chart: 'ct-chart-heat',
            horizontalBars: 'ct-horizontal-bars',
            label: 'ct-label',
            labelGroup: 'ct-labels',
            series: 'ct-series',
            cell: 'ct-cell',
            grid: 'ct-grid',
            gridGroup: 'ct-grids',
            seriesGroup: 'ct-series',
            gridBackground: 'ct-grid-background',
            vertical: 'ct-vertical',
            horizontal: 'ct-horizontal',
            start: 'ct-start',
            end: 'ct-end'
        }
    };

    function createChart(options) {
        this.svg = Chartist.createSvg(this.container, options.width, options.height, options.classNames.chart);

        const gridGroup = this.svg.elem('g').addClass(options.classNames.gridGroup);
        const seriesGroup = this.svg.elem('g');
        const labelGroup = this.svg.elem('g').addClass(options.classNames.labelGroup);

        const chartRect = Chartist.createChartRect(this.svg, options, defaultOptions.padding);
        const labelAxisX = new Chartist.StepAxis(Chartist.Axis.units.x, this.data.series, chartRect, {
            ticks: this.data.Xlabels
        });

        const labelAxisY = new Chartist.StepAxis(Chartist.Axis.units.y, this.data.series, chartRect, {
            ticks: this.data.Ylabels
        });

        const valueAxis = new Chartist.AutoScaleAxis(
            Chartist.Axis.units.y,
            this.data.series,
            chartRect,
            Chartist.extend({}, options.axisY, {
                highLow: 1,
                referenceValue: 0
            })
        );

        labelAxisX.createGridAndLabels(gridGroup, labelGroup, this.supportsForeignObject, options, this.eventEmitter);
        labelAxisY.createGridAndLabels(gridGroup, labelGroup, this.supportsForeignObject, options, this.eventEmitter);
        valueAxis.createGridAndLabels(gridGroup, labelGroup, this.supportsForeignObject, options, this.eventEmitter);

        const getColor = (value) => {
            const { colorRange } = this.data;
            const { color: colors } = colorRange;
            const result = colors.find((color) => {
                let { minValue, maxValue } = color;
                minValue = parseInt(minValue);
                maxValue = parseInt(maxValue);
                return value >= minValue && value < maxValue;
            });

            return result.colorCode;
        };

        this.data.series.forEach((series, seriesIndex) => {
            const seriesElement = seriesGroup.elem('g');
            seriesElement.addClass(options.classNames.series);
            seriesElement.addClass(series.className || `${options.classNames.series}-${Chartist.alphaNumerate(seriesIndex)}`);

            series.data.forEach((value, valueIndex) => {
                const { stepLength: stepXLength } = labelAxisX;
                const { stepLength: stepYLength } = labelAxisY;
                const { x1: beginningXPoint, y2: beginningYPoint } = chartRect;
                const metaData = {
                    x: stepXLength * valueIndex + beginningXPoint,
                    y: stepYLength * seriesIndex + beginningYPoint,
                    width: stepXLength - 2,
                    height: stepYLength - 8
                };

                const color = getColor(value);
                const $rect = seriesElement.elem('rect', metaData).addClass(options.classNames.cell).attr({
                    fill: color
                });

                $rect.attr({
                    'ct:series-name': series.name,
                    'ct:meta': Chartist.serialize(series.meta)
                });

                this.eventEmitter.emit(
                    'draw',
                    Chartist.extend(
                        {
                            type: 'cell',
                            value: value,
                            index: valueIndex,
                            series: series,
                            seriesIndex: seriesIndex,
                            axisX: labelAxisX,
                            axisY: labelAxisY,
                            chartRect: chartRect,
                            group: seriesElement,
                            element: $rect
                        },
                        metaData
                    )
                );
            });
        });

        this.eventEmitter.emit('created', {
            bounds: valueAxis.bounds,
            chartRect: chartRect,
            axisX: labelAxisX,
            axisY: labelAxisY,
            svg: this.svg,
            options: options
        });
    }

    function Heat(query, data, options, responsiveOptions) {
        Chartist.Heat.super.constructor.call(
            this,
            query,
            data,
            defaultOptions,
            Chartist.extend({}, defaultOptions, options),
            responsiveOptions
        );
    }

    Chartist.Heat = Chartist.Base.extend({
        constructor: Heat,
        createChart: createChart
    });
})(Chartist);
