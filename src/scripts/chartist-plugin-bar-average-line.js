(function (window, document, Chartist) {
    const defaultOptions = {
        value: [12, 5, 4, 7],
        columnWidth: 20,
    };

    function addAverageLine(data, options) {
        if (data.type === 'bar') {
            const yPosition = (options.value[data.index] * data.axisY.axisLength) / data.axisY.range.max;
            data.group.elem('line', {
                y1: data.chartRect.y1 - yPosition,
                y2: data.chartRect.y1 - yPosition,
                x1: data.x1 - options.columnWidth,
                x2: data.x1 + options.columnWidth,
                class: 'ct-grid ct-average-line',
            });

            data.element.attr({
                style: `stroke-width: ${options.columnWidth}`,
            });
        }
    }

    Chartist.plugins = Chartist.plugins || {};
    Chartist.plugins.ctBarAverageLine = (options) => {
        options = Chartist.extend({}, defaultOptions, options);
        return (chart) => {
            if (chart instanceof Chartist.Bar) {
                chart.on('draw', (data) => addAverageLine(data, options));
            }
        };
    };
})(window, document, Chartist);
