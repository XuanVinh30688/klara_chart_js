(function (Chartist) {
    var defaultOptions = {};
    var time = '.3 0.4 .2 1';

    Chartist.plugins = Chartist.plugins || {};
    Chartist.plugins.chartFullHeight = function (options) {
        options = Chartist.extend({}, defaultOptions, options);

        function addFullHeightLine(data) {
            data.group.elem('line', {
                x1: data.x1,
                x2: data.x2,
                y1: data.axisY.axisLength,
                y2: 0,
                class: 'ct-bar ct-bar-additional',
            });
        }

        function addFullHeightGrid(data, distance) {
            if (data.index !== 0 && data.index !== data.axis.ticks.length - 1) {
                data.group.elem('line', {
                    y1: data.y1,
                    y2: data.y2,
                    x1: data.x1 + distance / 2 - 4,
                    x2: data.x2 - distance / 2 + 4,
                    class: 'ct-grid ct-vertical ct-grid-additional',
                });
            }
        }

        return function chartFullHeight(chart) {
            if (!(chart instanceof Chartist.Bar)) {
                return;
            } else {
                let barDistance;
                chart.on('draw', function (data) {
                    chart.detach();
                    if (data.type === 'label' && data.axis.stepLength) {
                        barDistance = data.axis.stepLength;

                        data.element.attr({
                            style: 'max-width :' + barDistance + ';',
                        });
                    } else {
                        if (data.index === 0) {
                            data.element.attr({
                                style: 'display : none;',
                            });
                        }
                    }
                    if (data.type === 'bar') {
                        data.element.animate({
                            y2: {
                                dur: '0.3s',
                                from: data.y1,
                                to: data.y2,
                                keySplines: time,
                            },
                        });
                        addFullHeightLine(data);
                        data.element.attr({
                            style: 'stroke-width : 8px;',
                        });
                    }
                    if (data.type === 'grid') {
                        data.element.attr({
                            style: 'display : none;',
                        });
                        if (data.axis.units.dir === 'vertical') {
                            addFullHeightGrid(data, barDistance);
                        }
                    }
                });
            }
        };
    };
})(Chartist);
