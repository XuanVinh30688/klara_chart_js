(function (window, document, Chartist) {
    'use strict';

    var defaultOptions = {
        position: 'bottom',
        custom: null,
    };

    Chartist.plugins = Chartist.plugins || {};

    Chartist.plugins.legend = function (options) {
        var cachedDOMPosition;
        // Catch invalid options
        if (options && options.position) {
            if (!(options.position === 'top' || options.position === 'bottom' || options.position instanceof HTMLElement)) {
                throw Error('The position you entered is not a valid position');
            }
            if (options.position instanceof HTMLElement) {
                cachedDOMPosition = options.position;
                delete options.position;
            }
        }

        options = Chartist.extend({}, defaultOptions, options);

        if (cachedDOMPosition) {
            options.position = cachedDOMPosition;
        }

        return function legend(chart) {
            function removeLegendElement() {
                var legendElement = chart.container.querySelector('.ct-legend');
                if (legendElement) {
                    legendElement.parentNode.removeChild(legendElement);
                }
            }

            function createLegendElement() {
                var $legendElement = $('<ul>');
                $legendElement.addClass('ct-legend');
                if (chart instanceof Chartist.Pie) {
                    $legendElement.addClass('ct-legend-inside');
                }
                return $legendElement;
            }

            function getLegendItems() {
                var legendItems = [];
                var i;

                if (chart instanceof Chartist.Pie) {
                    legendItems = new Array(chart.data.labels.length);

                    for (i = 0; i < chart.data.labels.length; i++) {
                        legendItems[i] = {
                            label: chart.data.labels[i],
                            value: chart.data.series[i],
                        };
                    }

                    return legendItems;
                }

                legendItems = new Array(chart.data.series);

                for (i = 0; i < chart.data.series.length; i++) {
                    legendItems[i] = {
                        label: chart.data.series[i].name || chart.data.series[i],
                    };
                }

                return legendItems;
            }

            function appendLegendToDOM($legendElement) {
                if (!(options.position instanceof HTMLElement)) {
                    switch (options.position) {
                        case 'top':
                            $(chart.container).prepend($legendElement);
                            break;
                        case 'bottom':
                            $(chart.container).append($legendElement);
                            break;
                    }
                } else {
                    $(options.position).insertBefore($legendElement);
                }
            }

            function createLegendItemElement(i, legend) {
                var $li = $('<li>');
                $li.addClass('ct-series-' + i);
                $li.attr('data-legend', i);

                if (options.custom) {
                    $li.append(options.custom(i, legend));
                    return;
                }

                var $legendItemWrapper = $('<div class="ct-legend-item">');
                var $labelElement = $('<div class="ct-legend-label">');
                $labelElement.text(legend.label);
                $legendItemWrapper.append($labelElement);

                if (chart instanceof Chartist.Pie) {
                    var $valueElement = $('<div class="ct-legend-value">');
                    $valueElement.text(legend.value);
                    $legendItemWrapper.append($valueElement);
                }

                $li.append($legendItemWrapper);
                return $li;
            }

            removeLegendElement();

            var $legendElement = createLegendElement();
            var legendItems = getLegendItems();

            legendItems.forEach(function (legend, i) {
                var $legendItem = createLegendItemElement(i, legend);
                $legendElement.append($legendItem);
            });

            chart.on('created', function () {
                appendLegendToDOM($legendElement);
            });
        };
    };
})(window, document, Chartist);
