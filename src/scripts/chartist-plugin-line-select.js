(function (window, document, Chartist) {
    'use strict';

    var defaultOptions = {
        id: '',
        tooltipClass: 'ct-plugin-line-select-tooltip',
        tooltipShowClass: 'ct-plugin-line-select-tooltip__show',
        markerClass: 'ct-plugin-line-select-marker',
        markerShowClass: 'ct-plugin-line-select-marker__show',
        selectedBoxClass: 'ct-plugin-line-select-selected-box',
        selectedBoxShowClass: 'ct-plugin-line-select-selected-box__show',
        markerX: false,
        markerY: false,
        /* Function to merge the two found nearest points.
         * Can be one of: nearest, left, right, interpolate
         * Or a function. See comment over merge_functions.
         * Warning: interpolate doesn't properly work with lineSmooth: true. (TODO)
         * */
        mergeFnc: 'nearest',
        /* Function to create/update the tooltip content.
         * function fn(series, tooltip)...
         *  series contains {name: seriesname, value: value choosen by tooltipMergeFnc} for each series in the graph.
         *  If the return value is not null set it as textContent. tooltip can be used to update html.
         *  this is the plguin object which provides:
         *    options           : The option object
         *    merge_functions   : Object containing all build in merge function
         *    unProjectX(value) : Transforms x data value to svg position
         *    unProjectY(value) : Transforms y data value to svg position
         *    project(value)    : Transforms x svg position to x data value
         */
        /* Adds the tooltipHighlightPointClass to each point returned by the merge function if set. */
        highlightPoint: true,
        highlightPointClass: 'ct-plugin-line-select-tooltip-point-hit',
        /* Format function for series names. Used by the default tooltipMergeFnc */
        tooltipContentFormat: null,
        selectedX: null,
        onSelected: null,
    };

    function binarySearch(ar, el, compare_fn) {
        var m = 0;
        var n = ar.length - 1;
        while (m <= n) {
            var k = (n + m) >> 1;
            var cmp = compare_fn(el, ar[k]);
            if (cmp > 0) {
                m = k + 1;
            } else if (cmp < 0) {
                n = k - 1;
            } else {
                return k;
            }
        }
        return m - 1;
    }

    Chartist.plugins = Chartist.plugins || {};
    Chartist.plugins.LineSelect = function (options) {
        options = Chartist.extend({}, defaultOptions, options);

        var plugin = function lineSelect(chart) {
            var tooltipId = options.id + 'CTPluginLineTooltipId';
            var tooltip = document.getElementById(tooltipId);
            var chart_container = chart.container;
            var svg = null;
            var series = [];
            var created = false;
            var marker = null;
            var marker_y = null;
            var axis_x = null;
            var axis_y = null;
            var selectedBox = null;

            plugin.options = options;

            var merge_functions = (plugin.merge_functions = {
                left: function (left, right, point) {
                    if (!left) left = right;
                    return left;
                },
                right: function (left, right, point) {
                    if (!right) right = left;
                    return right;
                },
                nearest: function (left, right, point) {
                    if (!left) return right;
                    if (!right) return left;
                    point = project(point);
                    if (Math.abs(left.x - point) < Math.abs(right.x - point)) return left;
                    return right;
                },
                interpolate: function (left, right, point) {
                    if (!left) return right;
                    if (!right) return left;
                    point = project(point);
                    var ns = right.x - left.x;
                    var ls = Math.abs(left.x - point) / ns;
                    var rs = Math.abs(right.x - point) / ns;
                    return {
                        x: point,
                        y: right.y * ls + left.y * rs,
                    };
                },
            });

            var tooltipMergeFnc = options.mergeFnc;
            if (typeof tooltipMergeFnc !== 'function') {
                tooltipMergeFnc = merge_functions[tooltipMergeFnc];
            }

            if (!tooltip) {
                tooltip = document.createElement('div');
                tooltip.id = tooltipId;
                tooltip.className = options.tooltipClass;
                document.body.append(tooltip);
            }

            function get_nearest_points(elements, point) {
                var i = binarySearch(elements.raw_x, point, function (a, b) {
                    if (a > b) return 1;
                    if (a < b) return -1;
                    return 0;
                });
                var data = elements.data.data;
                var right = null;
                i = Math.max(0, i);
                var left = data[i];
                if (i + 1 < data.length) {
                    right = data[i + 1];
                }
                return [left, right];
            }

            var _tooltip_visible = false;
            var rafQueued = false;
            var last_event = null;

            function getSelectedPoints(svg_pos) {
                var values = [];

                for (var i = 0; i < series.length; ++i) {
                    var points = get_nearest_points(series[i], svg_pos.x);
                    points = tooltipMergeFnc(points[0], points[1], svg_pos.x);
                    values.push({
                        value: points,
                        series: series[i],
                    });
                }

                return values;
            }

            function updateSelectedBoxPosition(selectPoints) {
                var selectedBoxX = selectPoints.reduce(function (p, v) {
                    return p.value.x > v.value.x ? p : v;
                }).value.x;

                var selectedBoxY = selectPoints.reduce(function (p, v) {
                    return p.value.y > v.value.y ? p : v;
                }).value.y;

                var selectedBoxHeightY = selectPoints.reduce(function (p, v) {
                    return p.value.y < v.value.y ? p : v;
                }).value.y;

                var selectedBoxPositionX = unProjectX(selectedBoxX) - 16;
                var selectedBoxPositionY = unProjectY(selectedBoxY) - 16;
                var selectedBoxHeight = unProjectY(selectedBoxHeightY) - selectedBoxPositionY + 16;
                selectedBox.getNode().setAttribute('transform', 'translate(' + selectedBoxPositionX + ' ' + selectedBoxPositionY + ')');
                selectedBox.getNode().style.height = selectedBoxHeight + 'px';
            }

            function updateTooltipPosition(selectedPoints) {
                if (options.tooltipContentFormat) {
                    tooltip.textContent = options.tooltipContentFormat(selectedPoints);
                } else {
                    tooltip.textContent = selectedPoints[0].value.x;
                }

                $(tooltip).position({
                    my: 'center top',
                    at: 'center bottom',
                    of: selectedBox.getNode(),
                    collision: 'flip',
                });
            }

            function updateMakerPosition(svg_pos) {
                marker.getNode().setAttribute('transform', 'translate(' + svg_pos.x + ' 0)');
                if (options.markerY) {
                    marker_y.getNode().setAttribute('transform', 'translate(0 ' + svg_pos.y + ')');
                }
            }

            function highlightPoint(selectedPoints) {
                if (!options.highlightPoint) {
                    return;
                }

                for (var i = 0; i < selectedPoints.length; ++i) {
                    var p = selectedPoints[i].value;
                    selectedPoints[i].series.point
                        .getNode()
                        .setAttribute('transform', 'translate(' + unProjectX(p.x) + ' ' + unProjectY(p.y) + ')');
                }
            }

            function _showSelected() {
                if (!_tooltip_visible) {
                    showTooltip();
                }

                var _selectedX = options.selectedX || 0;
                var _selectedY = options.selectedY || 0;
                var svg_pos = {
                    x: unProjectX(_selectedX),
                    y: unProjectY(_selectedY),
                };
                var selectedPoints = getSelectedPoints(svg_pos);
                updateSelectedBoxPosition(selectedPoints);
                updateTooltipPosition(selectedPoints);
                highlightPoint(selectedPoints);
            }

            function showTooltip() {
                tooltip.classList.add(options.tooltipShowClass);
                selectedBox.addClass(options.selectedBoxShowClass);

                if (options.markerX) {
                    marker.addClass(options.markerShowClass);
                }
                if (options.markerY) {
                    marker_y.addClass(options.markerShowClass);
                }

                _tooltip_visible = true;
            }

            function hideTooltip() {
                tooltip.classList.remove(options.tooltipShowClass);
                selectedBox.removeClass(options.selectedBoxShowClass);
                marker.removeClass(options.markerShowClass);

                if (options.markerX) {
                    marker.removeClass(options.markerShowClass);
                }
                if (options.markerY) {
                    marker_y.removeClass(options.markerShowClass);
                }
            }

            chart_container.addEventListener('mouseenter', function (event) {
                showTooltip();
            });

            chart_container.addEventListener('mouseleave', function (event) {
                if (options.selectedX || options.selectedY) {
                    _showSelected();
                    return;
                }

                hideTooltip();
            });

            function _handleMousemove() {
                if (!_tooltip_visible) {
                    showTooltip();
                }

                var svg_pos = transformToSVG(last_event.pageX - window.pageXOffset, last_event.pageY - window.pageYOffset);
                var selectedPoints = getSelectedPoints(svg_pos);

                updateSelectedBoxPosition(selectedPoints);
                updateTooltipPosition(selectedPoints);
                updateMakerPosition(svg_pos);
                highlightPoint(selectedPoints);

                rafQueued = false;
            }

            chart_container.addEventListener('mousemove', function (event) {
                if (!created || rafQueued) {
                    return;
                }

                last_event = event;
                rafQueued = true;

                _handleMousemove();
            });

            function _handleClick() {
                if (!_tooltip_visible) {
                    showTooltip();
                }

                var svg_pos = transformToSVG(last_event.pageX - window.pageXOffset, last_event.pageY - window.pageYOffset);
                var selectedPoints = getSelectedPoints(svg_pos);

                options.selectedX = selectedPoints[0].value.x;
                options.selectedY = selectedPoints[0].value.y;

                updateSelectedBoxPosition(selectedPoints);
                updateTooltipPosition(selectedPoints);
                updateMakerPosition(svg_pos);
                highlightPoint(selectedPoints);

                if (typeof options.onSelected === 'function') {
                    options.onSelected(selectedPoints);
                }
            }

            chart_container.addEventListener('click', function (event) {
                last_event = event;
                _handleClick();
            });

            if (chart instanceof Chartist.Line) {
                chart.on('created', function (data) {
                    axis_x = data.axisX;
                    axis_y = data.axisY;
                    var svgElement = chart.svg.getNode();
                    svg = svgElement.tagName === 'svg' ? svgElement : svgElement.ownerSVGElement;

                    // TODO: there is prob. a nicer function inside chartist already for this.
                    // TODO: investigate and get rid of grid requirement.
                    var grid = chart.svg.querySelector('.ct-grids');
                    var grid_box = grid.getNode().getBoundingClientRect();
                    var cont_box = chart.container.getBoundingClientRect();
                    var top_padding = grid_box.top - cont_box.top;

                    selectedBox = chart.svg.elem(
                        'rect',
                        {
                            x1: 0,
                            y1: top_padding,
                            x2: 0,
                            y2: grid_box.height + top_padding,
                            rx: 15,
                            ry: 15,
                        },
                        options.selectedBoxClass,
                    );

                    marker = chart.svg.elem(
                        'line',
                        {
                            x1: 0,
                            y1: top_padding,
                            x2: 0,
                            y2: grid_box.height + top_padding,
                            style: options.markerClass,
                        },
                        options.markerClass,
                    );

                    if (options.markerY) {
                        var left_padding = grid_box.left - cont_box.left;
                        marker_y = chart.svg.elem(
                            'line',
                            {
                                x1: left_padding,
                                y1: 0,
                                x2: grid_box.width + left_padding,
                                y2: 0,
                                style: options.markerClass,
                            },
                            options.markerClass,
                        );
                    }

                    series = [];
                    var series_data = chart.data.series;
                    for (var i = 0; i < series_data.length; ++i) {
                        var raw_x_data = [];
                        var series_name = series_data[i].className
                            ? '.' + series_data[i].className
                            : '[*|series-name="' + series_data[i].name + '"]';
                        var elem = chart.svg.querySelector(series_name);
                        for (var j = 0; j < series_data[i].data.length; ++j) {
                            raw_x_data.push(unProjectX(series_data[i].data[j].x, axis_x));
                        }
                        var d = {
                            data: series_data[i],
                            raw_x: raw_x_data,
                            svg: elem,
                        };
                        if (options.highlightPoint) {
                            d.point = elem.elem(
                                'line',
                                {
                                    x1: 0,
                                    y1: 0,
                                    x2: 0.01,
                                    y2: 0,
                                },
                                chart.options.classNames.point + ' ' + options.highlightPointClass,
                            );
                        }
                        series.push(d);
                    }
                    created = true;

                    _showSelected();
                });
            }

            /*
             * Transforms screen coordinates into svg coordinates
             */
            var _point = null;

            function transformToSVG(x, y) {
                var matrix = svg.getScreenCTM();
                var point = _point || svg.createSVGPoint();
                point.x = x;
                point.y = y;
                point = point.matrixTransform(matrix.inverse());
                return (
                    point || {
                        x: 0,
                        y: 0,
                    }
                );
            }

            /*
             * Transforms x data value to svg position.
             */
            var unProjectX = (plugin.unProjectX = function (value) {
                var bounds = axis_x.bounds || axis_x.range;
                var max = bounds.max;
                var min = bounds.min;
                var range = bounds.range || max - min;
                return (axis_x.axisLength / range) * (value - min) + axis_x.chartRect.x1;
            });

            /*
             * Transforms y data value to svg position.
             */
            var unProjectY = (plugin.unProjectY = function (value) {
                var bounds = axis_y.bounds || axis_y.range;
                var max = bounds.max;
                var min = bounds.min;
                var range = bounds.range || max - min;
                return axis_y.chartRect.y1 - (axis_y.axisLength / range) * (value - min);
            });

            /*
             * Transforms x svg position to x data value..
             */
            var project = (plugin.project = function (value) {
                var bounds = axis_x.bounds || axis_x.range;
                var max = bounds.max;
                var min = bounds.min;
                var range = bounds.range || max - min;
                return ((value - axis_x.chartRect.x1) * range) / axis_x.axisLength + min;
            });
        };
        return plugin;
    };
})(window, document, Chartist);
