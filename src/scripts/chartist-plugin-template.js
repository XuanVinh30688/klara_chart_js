(function (window, document, Chartist) {
    'use strict';

    var defaultOptions = {};

    Chartist.plugins = Chartist.plugins || {};
    Chartist.plugins.tooltip = function (options) {
        options = Chartist.extend({}, defaultOptions, options);

        return function tooltip(chart) {};
    };
})(window, document, Chartist);
