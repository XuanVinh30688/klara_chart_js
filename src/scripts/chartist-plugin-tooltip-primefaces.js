(function (window, document, Chartist) {
    'use strict';
    Chartist.plugins = Chartist.plugins || {};
    Chartist.plugins.tooltipCustom = function (options) {
        return function tooltipCustom(chart) {
            var defaultOptions = {
                position: 'top',
                activatedByClick: true,
                trackMouse: false,
                custom: null,
            };

            options = Chartist.extend({}, defaultOptions, options);

            var tooltipHandler = {
                cfg: {},
                init: function (chart, options) {
                    this.cfg.custom = options.custom;
                    this.cfg.trackMouse = options.trackMouse;
                    this.cfg.position = options.position;
                    this.cfg.activatedByClick = options.activatedByClick;
                    this.cfg.showEvent = 'mouseover.tooltip';
                    this.cfg.hideEvent = 'mouseout.tooltip';
                    this.cfg.clickEvent = 'click.tooltip';

                    this.jq = $('#ctTooltip');

                    if (!this.jq.length) {
                        this.jq = $(
                            '<div id="ctTooltip" class="ct-tooltip ct-tooltip-global ui-widget ct-tooltip-' +
                                options.position +
                                '"></div>',
                        ).appendTo('body');
                        this.jq.append(
                            '<div class="ct-tooltip-arrow"></div><div class="ct-tooltip-text ui-shadow ui-corner-all"></div>',
                        );
                    }

                    this.chart = chart;
                    this.cfg.position = options.position;
                    this.bindTarget();
                },

                alignUsing: function (position, feedback) {
                    this.jq.removeClass('ct-tooltip-left ct-tooltip-right ct-tooltip-top ct-tooltip-bottom');

                    switch (this.cfg.position) {
                        case 'right':
                        case 'left':
                            this.jq.addClass('ct-tooltip-' + (feedback.horizontal == 'left' ? 'right' : 'left'));
                            break;
                        case 'top':
                        case 'bottom':
                            this.jq.addClass('ct-tooltip-' + (feedback.vertical == 'top' ? 'bottom' : 'top'));
                            break;
                    }

                    var box = this.target.getBoundingClientRect();

                    this.jq.css({
                        left: position.left,
                        top: position.top,
                    });
                },

                align: function () {
                    var $this = this;
                    this.jq.css({
                        left: '',
                        top: '',
                    });

                    if (this.cfg.trackMouse && this.mouseEvent) {
                        this.jq.position({
                            my: 'left top+15',
                            at: 'right bottom',
                            of: this.mouseEvent,
                            collision: 'flipfit',
                            using: function (p, f) {
                                $this.alignUsing.call($this, p, f);
                            },
                        });

                        this.mouseEvent = null;
                        return;
                    }

                    var _my, _at;

                    switch (this.cfg.position) {
                        case 'center':
                            _my = 'center center';
                            _at = 'center center';
                            break;

                        case 'right':
                            _my = 'left center';
                            _at = 'right center';
                            break;

                        case 'left':
                            _my = 'right center';
                            _at = 'left center';
                            break;

                        case 'top':
                            _my = 'center bottom';
                            _at = 'center top';
                            break;

                        case 'bottom':
                            _my = 'center top';
                            _at = 'center bottom';
                            break;
                    }

                    this.jq.position({
                        my: _my,
                        at: _at,
                        of: this.mouseEvent,
                        collision: 'flipfit',
                        using: function (p, f) {
                            $this.alignUsing.call($this, p, f);
                        },
                    });
                },

                show: function () {
                    var $this = this;
                    this.align();
                    if (this.cfg.trackMouse) {
                        this.followMouse();
                    }
                    this.jq.show();
                },

                hide: function () {
                    var $this = this;

                    if (this.isVisible()) {
                        this.jq.hide(null, function () {
                            if ($this.cfg.trackMouse) {
                                $this.unfollowMouse();
                            }
                        });
                    }
                },

                toggle: function () {
                    if (this.isVisible()) {
                        this.hide();
                    } else {
                        this.show();
                    }
                },

                isVisible: function () {
                    return this.jq.is(':visible');
                },

                bindTarget: function () {
                    var $this = this;

                    if (this.cfg.activatedByClick) {
                        $(this.chart.container)
                            .off(this.cfg.clickEvent)
                            .on(this.cfg.clickEvent, function (e) {
                                if ($(e.target).not('.ct-point, .ct-bar, .ct-slice-pie, .ct-slice-donut').length) {
                                    $this.hide();
                                    $($this.chart.container)
                                        .find('.ct-line, .ct-point, .ct-bar, .ct-slice-pie, .ct-slice-donut')
                                        .removeClass('ct-tooltip-blur');
                                }
                            })
                            .on(
                                this.cfg.clickEvent,
                                '.ct-point, .ct-bar, .ct-slice-pie, .ct-slice-donut',
                                function (e) {
                                    if ($this.target === e.target) {
                                        $this.target = null;
                                        $this.hide();

                                        $($this.chart.container)
                                            .find('.ct-line, .ct-point, .ct-bar, .ct-slice-pie, .ct-slice-donut')
                                            .removeClass('ct-tooltip-blur');
                                        return;
                                    }
                                    $this.mouseEvent = e;
                                    $this.target = e.target;

                                    $(e.target).removeClass('ct-tooltip-blur');

                                    $($this.chart.container)
                                        .find('.ct-line, .ct-point, .ct-bar, .ct-slice-pie, .ct-slice-donut')
                                        .not(e.target)
                                        .addClass('ct-tooltip-blur');

                                    var $point = $(this);
                                    var tooltipModel = {
                                        value: $point.attr('ct:value'),
                                        seriesName: $point.parent().attr('ct:series-name'),
                                    };
                                    $this.buildContent(tooltipModel);

                                    $this.show();
                                },
                            );

                        var resizeNS = 'resize.chart-tooltip';
                        $(window)
                            .unbind(resizeNS)
                            .bind(resizeNS, function () {
                                $this.hide();
                                $($this.chart.container)
                                    .find('.ct-line, .ct-point, .ct-bar, .ct-slice-pie, .ct-slice-donut')
                                    .fadeTo('fast', 1);
                            });

                        return;
                    }

                    $(this.chart.container)
                        .off(this.cfg.showEvent + ' ' + this.cfg.hideEvent)
                        .on(this.cfg.showEvent, '.ct-point, .ct-bar, .ct-slice-pie, .ct-slice-donut', function (e) {
                            $this.mouseEvent = e;
                            $this.target = e.target;

                            $(e.target).fadeTo('fast', 1);

                            $($this.chart.container)
                                .find('.ct-line, .ct-point, .ct-bar, .ct-slice-pie, .ct-slice-donut')
                                .not(e.target)
                                .fadeTo('fast', 0.2);

                            var $point = $(this);
                            var tooltipModel = {
                                value: $point.attr('ct:value'),
                                seriesName: $point.parent().attr('ct:series-name'),
                            };
                            $this.buildContent(tooltipModel);

                            $this.show();
                        })
                        .on(this.cfg.hideEvent, '.ct-point, .ct-bar, .ct-slice-pie, .ct-slice-donut', function () {
                            $this.hide();
                            $($this.chart.container)
                                .find('.ct-line, .ct-point, .ct-bar, .ct-slice-pie, .ct-slice-donut')
                                .fadeTo('fast', 1);
                        });
                },

                buildContent: function (tooltipModel) {
                    if (!tooltipModel) {
                        return;
                    }

                    var value = tooltipModel.value;
                    var seriesName = tooltipModel.seriesName;

                    if (this.cfg.custom) {
                        this.jq.children('.ct-tooltip-text').html(this.cfg.custom(tooltipModel));
                        return;
                    }

                    this.jq.children('.ct-tooltip-text').text(value);
                },

                followMouse: function () {
                    var $this = this;

                    $(this.target).on('mousemove.tooltip-track', function (e) {
                        $this.jq.position({
                            my: 'left top+15',
                            at: 'right bottom',
                            of: e,
                            collision: 'flipfit',
                        });
                    });
                },

                unfollowMouse: function () {
                    $(this.target).off('mousemove.tooltip-track');
                },
            };

            tooltipHandler.init(chart, options);
        };
    };
})(window, document, Chartist);
