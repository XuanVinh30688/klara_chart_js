const path = require('path')
const { CleanWebpackPlugin } = require('clean-webpack-plugin')
const HtmlWebpackPlugin = require('html-webpack-plugin')
const MiniCssExtractPlugin = require('mini-css-extract-plugin')
const fs = require('fs')
const DEV_MODE = process.env.npm_lifecycle_event == 'start'

function getAllFiles(folderPath, hasFullPath = true) {
    const baseDir = path.resolve(__dirname, folderPath)
    return fs.readdirSync(baseDir).map((item) => {
        if (hasFullPath) {
            return path.resolve(baseDir, item)
        }

        return item
    })
}

module.exports = {
    entry: {
        mainjs: getAllFiles('src/scripts'),
        maincss: path.resolve(__dirname, 'src/styles/main.scss'),
    },
    output: {
        path: path.resolve('build'),
        filename: '[name].js',
    },
    plugins: [
        new CleanWebpackPlugin(),
        ...getAllFiles('public', false).map((file) => {
            return new HtmlWebpackPlugin({
                filename: file,
                template: path.resolve(__dirname, 'public', file),
            })
        }),
        new MiniCssExtractPlugin({
            filename: `[name]${DEV_MODE ? '' : '.min'}.css`,
        }),
    ],
    module: {
        rules: [
            {
                test: /\.js$/,
                loader: 'babel-loader',
                options: {
                    compact: true,
                },
            },
            {
                test: /\.(scss|css)/,
                use: [
                    {
                        loader: MiniCssExtractPlugin.loader,
                    },
                    {
                        loader: 'css-loader',
                        options: {
                            sourceMap: DEV_MODE,
                        },
                    },
                    {
                        loader: 'sass-loader',
                        options: {
                            sourceMap: DEV_MODE,
                        },
                    },
                ],
            },
        ],
    },
}
